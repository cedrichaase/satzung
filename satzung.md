# Erfa Chaos Siegen Satzung

## Präambel

Die Informationsgesellschaft unserer Tage ist ohne Computer nicht mehr denkbar. Die Einsatzmöglichkeiten der automatisierten Datenverarbeitung und Datenübermittlung bergen Chancen, aber auch Gefahren für den Einzelnen und für die Gesellschaft. Informations- und Kommunikationstechnologien verändern das Verhältnis Mensch-Maschine und der Menschen untereinander.

Die Entwicklung zur Informationsgesellschaft erfordert ein neues Menschenrecht auf weltweite, ungehinderte Kommunikation. Der Chaos Computer Club (CCC) ist eine galaktische Gemeinschaft von Lebewesen, unabhängig von Alter, Geschlecht und Rasse sowie gesellschaftlicher Stellung, die sich grenzüberschreitend für Informationsfreiheit einsetzt und mit den Auswirkungen von Technologien auf die Gesellschaft sowie das einzelne Lebewesen beschäftigt und das Wissen um diese Entwicklung fördert.

Der Chaos Computer Club Siegen setzt die Ziele des CCC lokal um und ist die informelle Häufung von CCC-Mitgliedern im Hackspace Siegen e. V. (HaSi), mit dem Ziel das HaSi selber zu unterstützen, dem HaSi eine Schnittstelle zum CCC zu sein und dem CCC eine Schnittstelle zum HaSi zu sei.

## § 1 Name, Sitz, Geschäftsjahr
1. Der Verein führt den Namen "Chaos Computer Club Siegen", kürzer "Chaos Siegen", als Kürzel "CCCSi", als Kurzkürzel "c3Si", auch "<3Si". Er soll in das Vereinsregister eingetragen werden und trägt dann den Zusatz „e. V.“ Im Folgenden wird "Club" synonym mit diesem Verein verwendet.
2. Der Sitz des Vereins ist Siegen.
3. Das Geschäftsjahr ist das Kalenderjahr. Das erste Geschäftsjahr beginnt mit der Eintragung des Vereins in das Vereinsregister und endet am 31.12. diesen Jahres. 

## § 2 Zweck und Selbstlosigkeit
1. Aus der Präambel sind die folgenden Zwecke vorgegeben:
    1. "Der Chaos Computer Club Siegen setzt die Ziele des CCC lokal um." Die vom CCC übernommene Ziele sind:
       1. Regelmäßige Öffentliche Treffen und Informationsveranstaltungen.
       2. Veranstaltungen und/oder Förderung internationaler Kongresse, Treffen sowie Konferenzen.
       3. Herausgabe multimedialer Publikationen
       4. Öffentlichkeits- und Lobbyarbeit
       5. Arbeits- und Erfahrungsaustausch-Kreise
       6. Informationsaustausch mit den in der Datenschutzgesetzgebung vorgesehenen Kontrollorganen.
       7. Förderung des schöpferisch-kritischen Umgangs mit Technologie.
       8. Hilfestellung und Beratung bei technischen und rechtlichen Fragen im Rahmen der gesetzlichen Möglichkeiten für die Mitglieder.
       9. Zusammenarbeit und Austausch mit nationalen und internationalen Gruppierungen, deren Ziele mit denen des Clubs vereinbar sind.
       10. Veranstaltungen und Projekte, die sich speziell an Jugendliche richten.
    2. "das HaSi selber zu unterstützen". Die vom HaSi übernommenen Ziele sind: 
        1. Gemeinschaftliche Entwicklung von Software sowie elektronischen Schaltungen, 
        2. Information der Öffentlichkeit
        3. Anleitung zum kritischen Umgang mit elektronischen Medien, Software und Hardware.
    3. "dem HaSi eine Schnittstelle zum CCC zu sein und dem CCC eine Schnittstelle zum HaSi zu sei." Die sich daraus ergebene Ziele sind:
        1. Anerkennung als Erfahrungsaustausch-Kreis (Erfa) Siegen des CCC
        2. Fördern und Vorbereiten der Entscheidungsfindung im CCC
        3. Mitgliederwerbung für den CCC
        4. Ziele des CCC lokal umzusetzen. 
1. Der Verein ist selbstlos tätig.
    1. Er verfolgt nicht in erster Linie eigenwirtschaftliche Zwecke.
    2. Mittel des Vereins dürfen nur für die satzungsmäßigen Zwecke verwendet werden. 
    3. Alle Inhaber von Vereinsämtern sind ehrenamtlich tätig.
    4. Die Mitglieder erhalten keine Gewinnanteile und in ihrer Eigenschaft als Mitglieder auch keine sonstigen Zuwendungen aus Mitteln des Vereins.
    5. Es darf keine Person durch Ausgaben, die dem Zweck der Körperschaft fremd sind, oder durch unverhältnismäßig hohe Vergütungen begünstigt werden.

## § 3 Mitgliedschaft
1. Ordentliche Mitglieder können natürliche Personen werden, die eine Mitgliedschaft im CCC und im HaSi nachweisen können.
2. Bei Minderjährigen bedarf es darüber hinaus der Zustimmung der Erziehungsberechtigten.
3. Der Aufnahmeantrag ist schriftlich oder fernschriftlich beim Vorstand zu stellen. 
4. Über den Aufnahmeantrag entscheidet die Mitgliederversammlung.
5. Die Mitgliedschaft beginnt mit der Annahme des Aufnahmeantrags.
6. Im Falle einer nicht fristgerechten Entrichtung der Beiträge ruht die Mitgliedschaft und damit das Stimmrecht in der Mitgliederversammlung.
7. Fördermitglieder können natürliche und juristische Personen, nicht rechtsfähige Vereine, sowie Anstalten und Körperschaften des öffentlichen Rechts werden.
8. Fördermitglieder sind passive Mitglieder ohne Stimmrecht in der Mitgliederversammlung.
8. Die Mitgliedschaft endet durch Austrittserklärung, durch Tod von natürlichen Personen oder durch Auflösung und Erlöschen von juristischen Personen, nicht rechtsfähigen Vereinen, sowie Anstalten und Körperschaften des öffentlichen Rechts oder durch Ausschluss.
9. Der Austritt erfolgt durch schriftliche Erklärung gegenüber dem Vorstand.
10. Die Beitragspflicht für das laufende Geschäftsjahr bleibt hiervon unberührt.
11. Ein Mitglied kann durch Beschluss des Vorstandes ausgeschlossen werden, wenn es das Ansehen des Vereins schädigt, seinen Beitragsverpflichtungen nicht nachkommt oder wenn ein sonstiger wichtiger Grund vorliegt. 
12. Wichtige Gründe sind insbesondere ein die Vereinsziele schädigendes Verhalten, die Verletzung satzungsmäßiger Pflichten oder Beitragsrückstände von mindestens einem Jahr.
13. Der Vorstand muss dem auszuschließenden Mitglied den Beschluss in schriftlicher Form unter Angabe von Gründen mitteilen und ihm auf Verlangen eine Anhörung gewähren.
14. Gegen den Ausschluss steht der auszuschließenden Person die Berufung an die Mitgliederversammlung zu, die schriftlich binnen eines Monats an den Vorstand zu richten ist.
15. Bis zum Beschluss der Mitgliederversammlung ruht die Mitgliedschaft.
16. Die Mitgliederversammlung entscheidet im Rahmen des Vereins endgültig.
17. Dem Mitglied bleibt die Überprüfung der Maßnahme durch Anrufung der ordentlichen Gerichte vorbehalten.
18. Die Anrufung eines ordentlichen Gerichts hat aufschiebende Wirkung bis zur Rechtskraft der gerichtlichen Entscheidung.

## § 4 Rechte und Pflichten der Mitglieder
1. Die Mitglieder sind berechtigt, die Leistungen des Vereins in Anspruch zu nehmen. 
2. Die Mitglieder sind verpflichtet, die satzungsgemäßen Zwecke des Vereins zu unterstützen und zu fördern. 
3. Sie sind verpflichtet, die festgesetzten Beiträge zu zahlen. 
5. Die Mitglieder sind verpflichtet ihre erreichbare E-Mail-Adresse sowie ihre postalische Anschrift anzugeben und bei Änderung diese dem Vereinsvorstand gegenüber mitzuteilen.

## § 5 Beiträge
1. Der Club erhebt Mitgliedsbeiträge. Das Nähere regelt eine Beitragsordnung, die von der Mitgliederversammlung beschlossen wird. Im Falle nicht fristgerechter Entrichtung der Beiträge ruht die Mitgliedschaft.
2. Im begründeten Einzelfall kann für ein Mitglied durch Vorstandsbeschluss ein von der Beitragsordnung abweichender Beitrag festgesetzt werden.

## § 6 Organe des Clubs
Organe des Vereins sind die Mitgliederversammlung und der Vorstand.

## § 7 Mitgliederversammlung
1. Die Mitgliederversammlung ist das oberste Beschlussorgan. Zu ihren Aufgaben gehören 
    1. die Wahl der einzelnen Vorstandsmitglieder
    2. die Entgegennahme der Berichte und die Entlastung des Vorstandes, 
    3. die Genehmigung des Haushaltsplanes des Vorstandes,
    4. die Aufnahme und Ausschluss von Mitgliedern,
    5. die Bestellung von Finanzprüfern,
    6. die Genehmigung der Beitragsordnung,
    7. die Richtlinie über die Erstattung von Reisekosten und Auslagen,
    8. die Satzungsänderung,
    9. die Vereinsauflösung,
    10. weitere Aufgaben, soweit sich diese aus der Satzung oder nach dem Gesetz ergeben.
2. Im erstem Quartal eines jeden Geschäftsjahres findet eine ordentliche Mitgliederversammlung statt.
3. Der Vorstand ist zur Einberufung einer außerordentlichen Mitgliederversammlung verpflichtet, wenn 
mindestens ein Drittel der Mitglieder dies schriftlich unter Angabe von Gründen verlangt.
4. Die Einberufung der Mitgliederversammlung erfolgt in Textform durch den Vorstand mit einer Frist von
 mindestens zwei Wochen. Zur Wahrung der Frist reicht die Aufgabe der Einladung zur Post an die letzte 
 bekannte Anschrift oder die Versendung an die zuletzt bekannte E-Mail-Adresse. Der Einladung ist der 
 Tagesordnung beizufügen.
5. Die Tagesordnung ist zu ergänzen, wenn dies ein Mitglied bis spätestens eine Woche vor dem 
angesetzten Termin schriftlich beantragt. Die Ergänzung ist zu Beginn der Versammlung bekannt zu machen.
6. Anträge über die Abwahl des Vorstands, über die Änderung der Satzung und über die Auflösung des 
Vereins, die den Mitgliedern nicht bereits mit der Einladung zur Mitgliederversammlung zugegangen sind, können erst auf der nächsten Mitgliederversammlung beschlossen werden.
7. Die Mitgliederversammlung ist ohne Rücksicht auf die Zahl der erschienenen Mitglieder beschlussfähig.
10. Jedes ordentliche Mitglied hat eine Stimme.
11. Bei Abstimmungen entscheidet die einfache Mehrheit der abgegebenen Stimmen.
12. Satzungsänderungen und Auflösung des Vereins können nur mit einer Mehrheit von 2/3 der anwesenden Mitglieder beschlossen werden.
13. Stimmenthaltungen und ungültige Stimmen bleiben außer Betracht.
14. Über die Beschlüsse der Mitgliederversammlung ist ein Protokoll anzufertigen, das vom Versammlungsleiter und dem Schriftführer zu unterzeichnen ist.
15. Beschlüsse sind fortlaufend nummeriert und in einem Ordner aufzubewahren.

## § 8 Vorstand
1. Der Vorstand besteht aus dem Vorsitzenden (auch "Erster Hut"), dem Schatzmeister (auch "Finanz-Hut") und dem Erfa-Kreis-Vertreter.
2. Vorstand im Sinne des § 26, Abs. 2 BGB ist jedes Vorstandsmitglied. Ausgenommen sind Rechtsgeschäfte 
von über Euro 500,-, Einstellung und Entlassung von Angestellten, gerichtliche Vertretung sowie 
Anzeigen, Aufnahme von Krediten.
3. Die Vorstandsmitglieder sind grundsätzlich ehrenamtlich tätig; sie haben Anspruch
auf Erstattung notwendiger Auslagen im Rahmen einer von der Mitgliederversammlung zu
beschließenden Richtlinie über die Erstattung von Reisekosten und Auslagen.
4. Die Amtsdauer der Vorstandsmitglieder beträgt drei Jahre; Wiederwahl ist zulässig.
5. Ist mehr als ein Vorstandsmitglied dauernd an der Ausübung seines Amtes gehindert, so sind 
unverzüglich Nachwahlen anzuberaumen.
6. Der Vorstand ist Dienstvorgesetzter aller vom Club angestellten Mitarbeiter; er kann diese Aufgabe 
einem Vorstandsmitglied übertragen.
7. Der Schatzmeister überwacht die Haushaltsführung und verwaltet das Vermögen des Clubs. Er hat auf 
eine sparsame und wirtschaftliche Haushaltsführung hinzuwirken. Mit dem Ablauf des Geschäftsjahres stellt 
er unverzüglich die Abrechnung sowie die Vermögensübersicht und sonstige Unterlagen von 
wirtschaftlichen Belang den Finanzprüfern des Clubs zur Prüfung zur Verfügung.
8. Der Erfa-Kreis-Vertreter vertritt den Club im Erfa-Beirat des CCC. Er hat auf aktuelle Darstellung des
 Clubs im CCC-Kontext zu achten und auf regen Austausch mit den benachbarten Chaostreffs und Erfas 
 hinzuwirken. Beabsichtigt der Verein, bestimmte Themen oder Aktivitäten mit überregionalem Bezug an die Öffentlichkeit zu tragen, ist 
dies über den Erfa-Kreis-Vertreter mit dem Vorstand des Chaos Computer Clubs abzustimmen.

## § 9 Finanzprüfer
1. Zur Kontrolle der Haushaltsführung bestellt die Mitgliederversammlung einen oder mehrere Finanzprüfer. 
2. Nach Durchführung ihrer Prüfung erstatten sie dem Vorstand Kenntnis von ihrem Prüfungsergebnis und erstatten
 der Mitgliederversammlung Bericht.
2. Die Finanzprüfer dürfen dem Vorstand nicht angehören.
3. Wiederwahl ist zulässig.

## § 10 Auflösung des Vereins
Bei Auflösung, bei Entziehung der Rechtsfähigkeit des Vereins fällt das gesamte Vermögen an die Wau Holland Stiftung, die es ausschließlich und unmittelbar für gemeinnützige Zwecke zu verwenden hat. Sollte dieser Verein zu diesem Zeitpunkt nicht mehr gemeinnützig sein, fällt das Vermögen an eine andere von der Mitgliederversammlung zu bestimmende gemeinnützige Körperschaft, die das Vermögen für gemeinnützige Zwecke zu verwenden hat.

Siegen, den