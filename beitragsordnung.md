# Anlage 1 – "Beitragsordnung“

## § 1 Aufnahmebeitrag
* Ordentliches Mitglied: Der Club erhebt keinen Aufnahmebeitrag.
* Fördermitglied: Der Club erhebt keinen Aufnahmebeitrag.

## § 2 Mitgliedsbeitrag
Der Mitgliedsbeitrag gilt für das ganze Jahr. Bei Austritt wird der Beitrag des laufenden Jahres nicht erstattet.

* Ordentliches Mitglied: Der Club erhebt einen Jahresbeitrag in Höhe von 10 Euro, zu zahlen bei Eintritt oder in der ersten Januarwoche.
* Fördermitglied: Der Club erhebt einen Jahresbeitrag in Höhe von 10 EUR, zu zahlen bei Eintritt oder in der ersten Januarwoche.