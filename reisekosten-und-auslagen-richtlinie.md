# Anlage 2 – "Reisekosten- und Auslagenrichtlinie“

## § 1 Reisekosten

Für den Ersatz von Aufwendungen ist, soweit nicht andere gesetzliche Bestimmungen anzuwenden sind, das Bundesreisekostengesetz maßgebend.

## § 2 Auslagenrichtline

Für den Ersatz von Aufwendungen ist, soweit nicht andere gesetzliche Bestimmungen anzuwenden sind, das Bundesreisekostengesetz maßgebend.
